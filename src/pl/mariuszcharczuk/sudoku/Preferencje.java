package pl.mariuszcharczuk.sudoku;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class Preferencje extends PreferenceActivity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.ustawienia);
	}

}
