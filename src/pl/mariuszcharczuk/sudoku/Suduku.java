package pl.mariuszcharczuk.sudoku;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

import android.content.Intent;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.util.Log;

public class Suduku extends Activity implements OnClickListener{
	private static final String ZNACZNIK = "Sudoku";

	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sudiku);
        
	    // Konfigurujemy obiekty nas�uchuj�ce klikni�� dla wszystkich przycisk�w
	    View btnContinue = findViewById(R.id.btn_continue);
	    btnContinue.setOnClickListener(this);
	    View btnNowaGra = findViewById(R.id.btn_new_game);
	    btnNowaGra.setOnClickListener(this);
	    View btnInformacje = findViewById(R.id.btn_info);
	    btnInformacje.setOnClickListener(this);
	    View btnExit = findViewById(R.id.btn_exit);
	    btnExit.setOnClickListener(this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater wypelniacz = getMenuInflater();
        wypelniacz.inflate(R.menu.menu, menu);
        return true;
    }
    

    @Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.ustawienia:
			startActivity(new Intent(this, Preferencje.class));
			return true;
		}
		return false;
	}
    
    private void openNewGameDialog(){
    	new AlertDialog.Builder(this)
    		.setTitle(R.string.tytul_nowa_gra)
    		.setItems(R.array.trudnosc, 
    		new DialogInterface.OnClickListener() {				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					startGame(which);
				}
			})
			.show();
    }
    
    private void startGame(int lvl){
    	Log.d(ZNACZNIK, "Klikni�to: "+lvl);
    	Intent intencja = new Intent(Suduku.this, Gra.class);
        intencja.putExtra(Gra.TRUDNOSC_KLUCZ, lvl);
        startActivity(intencja);
    	
    }

	@Override
	public void onClick(View v) {
		 switch (v.getId()) {
	        case R.id.btn_info:
	            Intent i = new Intent(this, InformationPage.class);
	            startActivity(i);
	            break;
	        // Tutaj definiujemy wi�cej klawiszy (je�li s� jakie�)�
	            
	        case R.id.btn_new_game:
	        	openNewGameDialog();
	        	break;

	        case R.id.btn_exit:
	            finish();
	            break;
	        }		
	}
    
}
