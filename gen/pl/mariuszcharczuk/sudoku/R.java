/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package pl.mariuszcharczuk.sudoku;

public final class R {
    public static final class anim {
        public static final int cykl_7=0x7f040000;
        public static final int wstrzasy=0x7f040001;
    }
    public static final class array {
        public static final int trudnosc=0x7f0a0000;
    }
    public static final class attr {
    }
    public static final class color {
        public static final int puzzle_ciemny=0x7f070004;
        public static final int puzzle_jasny=0x7f070003;
        public static final int puzzle_pierwszy_plan=0x7f070005;
        public static final int puzzle_podpowiedz_0=0x7f070006;
        public static final int puzzle_podpowiedz_1=0x7f070007;
        public static final int puzzle_podpowiedz_2=0x7f070008;
        public static final int puzzle_podswietlenie=0x7f070002;
        public static final int puzzle_tlo=0x7f070001;
        public static final int puzzle_zaznaczony=0x7f070009;
        public static final int tlo=0x7f070000;
    }
    public static final class dimen {
        /**  Default screen margins, per the Android Design guidelines. 

         Customize dimensions originally defined in res/values/dimens.xml (such as
         screen margins) for sw720dp devices (e.g. 10" tablets) in landscape here.
    
         */
        public static final int activity_horizontal_margin=0x7f060000;
        public static final int activity_vertical_margin=0x7f060001;
    }
    public static final class drawable {
        public static final int ic_launcher=0x7f020000;
        public static final int sudoku_icon=0x7f020001;
    }
    public static final class id {
        public static final int  klawiatura_3=0x7f0c0008;
        public static final int  klawiatura_4=0x7f0c0009;
        public static final int  klawiatura_5=0x7f0c000a;
        public static final int  klawiatura_6=0x7f0c000b;
        public static final int  klawiatura_7=0x7f0c000c;
        public static final int  klawiatura_8=0x7f0c000d;
        public static final int  klawiatura_9=0x7f0c000e;
        public static final int action_settings=0x7f0c0010;
        public static final int btn_continue=0x7f0c0000;
        public static final int btn_exit=0x7f0c0003;
        public static final int btn_info=0x7f0c0002;
        public static final int btn_new_game=0x7f0c0001;
        public static final int klawiatura=0x7f0c0005;
        public static final int klawiatura_1=0x7f0c0006;
        public static final int klawiatura_2=0x7f0c0007;
        public static final int textView1=0x7f0c0004;
        public static final int ustawienia=0x7f0c000f;
    }
    public static final class layout {
        public static final int activity_sudiku=0x7f030000;
        public static final int information_page=0x7f030001;
        public static final int klawiatura=0x7f030002;
    }
    public static final class menu {
        public static final int menu=0x7f0b0000;
        public static final int sudiku=0x7f0b0001;
    }
    public static final class string {
        public static final int action_settings=0x7f080001;
        public static final int android_sudoku=0x7f080002;
        public static final int app_name=0x7f080000;
        public static final int btn_continue=0x7f080003;
        public static final int btn_exit=0x7f080006;
        public static final int btn_info=0x7f080005;
        public static final int btn_new_game=0x7f080004;
        public static final int etykieta_brak_ruchow=0x7f080015;
        public static final int etykieta_latwy=0x7f080011;
        public static final int etykieta_sredni=0x7f080012;
        public static final int etykieta_trudny=0x7f080013;
        public static final int etykieta_ustawienia=0x7f080009;
        public static final int podsumowanie_muzyka=0x7f08000d;
        public static final int podsumowanie_podpowiedzi=0x7f08000f;
        public static final int skrot_ustawienia=0x7f08000b;
        public static final int text_informacje=0x7f080008;
        public static final int tytul_gra=0x7f080014;
        public static final int tytul_informacje=0x7f080007;
        public static final int tytul_klawiatura=0x7f080016;
        public static final int tytul_muzyka=0x7f08000c;
        public static final int tytul_nowa_gra=0x7f080010;
        public static final int tytul_podpowiedzi=0x7f08000e;
        public static final int tytul_ustawienia=0x7f08000a;
    }
    public static final class style {
        /** 
        Base application theme, dependent on API level. This theme is replaced
        by AppBaseTheme from res/values-vXX/styles.xml on newer devices.
    

            Theme customizations available in newer API levels can go in
            res/values-vXX/styles.xml, while customizations related to
            backward-compatibility can go here.
        
         */
        public static final int AppBaseTheme=0x7f090000;
        /**  Application theme. 
 All customizations that are NOT specific to a particular API-level can go here. 
         */
        public static final int AppTheme=0x7f090001;
    }
    public static final class xml {
        public static final int ustawienia=0x7f050000;
    }
}
